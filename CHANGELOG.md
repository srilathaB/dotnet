## v0.4.2 (2021-01-15)

- Coverage report should be displayed in merge requests #17

## v0.4.1 (2021-01-06)

- Allow mutation testing job to fail on master branch #18

## v0.4.0 (2020-12-11)

- Update windows and linux images to .NET 5.0

## v0.3.0 (2020-11-17)

### New Features

- Add job or stage to sign binaries and nuget packages #11

### Bug Fixes

- Push stage should work with multiple packages #10

## v0.2.0 (2020-11-06)

### New Features

- Move default, stages, workflow, and variables to separate files #7
- Change artifact expiration on master branch #6

### Bug Fixes

- 'push to nuget dev' job should not run for releases #9

## v0.1.0 (2020-11-05)

Initial release of the dotnet CI project, which contains
the CI configuration taken from `v0.2.0` of
[reductech/sandbox/templates/dotnetlibrary](https://gitlab.com/reductech/sandbox/templates/dotnetlibrary/-/tags/v0.2.0)
and the following additional features.

### New Features

- Add caching for nuget packages #4
- Create new CI config for windows / rt runners #1

### Documentation

- Add readme with instructions on how to use the templates #2
